class Group < ActiveRecord::Base
  validates :api_key, presence: true
  validates :group_id, presence: true

  scope :enabled, ->() { where(enabled: true) }
end
