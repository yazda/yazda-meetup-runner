class GroupEventWorker
  include Sidekiq::Worker

  def perform(*args)
    Group.enabled.find_each do |group|
      MeetupEventWorker.perform_async group.id
    end
  end
end
