require 'RMeetup'

class MeetupEventWorker
  include Sidekiq::Worker

  def perform(*args)
    group    = Group.find args[0]
    now      = DateTime.now.to_s
    one_week = 1.week.from_now.to_s

    RMeetup::Client.api_key = group.api_key
    results                 = RMeetup::Client.fetch(:events, { group_id: group.group_id,
                                                               time:     "#{now},#{one_week}" })

    results.each do |event|
      persisted_event = Event.find_by_meetup_id event.id

      unless persisted_event
        CreateYazdaEventWorker.perform_async event
      end
    end
  end
end
