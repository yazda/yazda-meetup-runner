class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :api_key
      t.string :group_id
      t.boolean :enabled, default: true

      t.timestamps
    end
  end
end
